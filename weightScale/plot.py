import os
import datetime
import random
import dash
from dash import Dash, dcc, html, Input, Output, callback
import plotly
import csv
import time
from datetime import datetime 
import sys

# Specify the directory path where the csv file is
directory_path = './tmp'
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
#csv_file = sys.argv[1]
#csv_file = "2023-11-04-17-11-46.csv"

app = Dash(__name__, external_stylesheets=external_stylesheets)
app.layout = html.Div(
    html.Div([
        html.H4('ION live feed'),
        html.Div(id='live-update-text'),
        dcc.Graph(id='live-update-graph'),
        dcc.Interval(
            id='interval-component',
            interval=1*1000, # in milliseconds
            n_intervals=0
        )
    ])
)

# Multiple components can update everytime interval gets fired.
@callback(Output('live-update-graph', 'figure'),
              Input('interval-component', 'n_intervals'))

def update_graph_live(n):
    #satellite = Orbital('TERRA')
    data = {
        'time': [],
        'weight': []
    }

    # Get the list of file names in the directory
    file_names = os.listdir(directory_path)

    if len(file_names) == 0:
        raise ValueError("El archivo de datos no existe en el directorio " + directory_path)
    csv_file = directory_path + "/" + file_names[0]
    print("Graficando datos de: " + csv_file)

    # Collect some data
    with open(csv_file, 'r') as file:
        csv_reader = csv.DictReader(file)
    
        # Iterate through the rows in the CSV file and extract data from "name" and "data" columns
        for row in csv_reader:
            data['time'].append(row['Timestamp'])
            data['weight'].append(float(row['loadcell']))

    # Create the graph with subplots
    fig = plotly.tools.make_subplots(rows=1, cols=1, vertical_spacing=0.2)
    fig['layout']['margin'] = {
        'l': 30, 'r': 10, 'b': 30, 't': 10
    }
    fig['layout']['legend'] = {'x': 0, 'y': 1, 'xanchor': 'left'}

    fig.append_trace({
        'x': data['time'],
        'y': data['weight'],
        'name': 'Altitude',
        'mode': 'lines+markers',
        'type': 'scatter'
    }, 1, 1)

    return fig


if __name__ == "__main__":
    app.run(debug=True)