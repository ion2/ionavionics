import serial
import time
import io
import json
import csv
import time
import datetime
import subprocess
import shutil
import os

# Pines Negro, blanco, rojo

arduino = serial.Serial(port='COM7',   baudrate=9600 , timeout=.1)
sio = io.TextIOWrapper(io.BufferedRWPair(arduino, arduino))
tmp_directory_path = './tmp'
filename = tmp_directory_path + "/" + time.strftime("%Y-%m-%d-%H-%M-%S") + '.csv'

def initBluetooth():
    arduino.write("CMD".encode())

def read():
    data = arduino.readline()
    try:
        data = data.decode().strip()
    except ValueError:
        print("Error al decodificar")
        return None
    try:
        json_data = json.loads(data)
        return json_data
    except ValueError:
        if data != "":
            print(data)
    return None

def create_file():
    
    print("Creating csv file: ", filename)
    # Clean tmp directory
    file_names = os.listdir(tmp_directory_path)

    # Print the file names
    for name in file_names:
        print("Moving " + name + " to parent directory.")
        source_path = tmp_directory_path + "/" + name
        destination_path = './'
        shutil.move(source_path, destination_path)

    # Create the directory
    os.makedirs(tmp_directory_path, exist_ok=True)
    with open(filename, 'w', newline='') as file:
             writer = csv.writer(file)
             writer.writerow(["Timestamp", "loadcell"])
             file.close()



def update_file(data, current_time):
     load_cell_value = data["Load_cell"]     
     formatted = current_time.strftime('%H:%M:%S.%f')
     with open(filename, 'a', newline='') as file:
             writer = csv.writer(file)
             writer.writerow([formatted, load_cell_value])

def start_plot():
    command = ["python", "debug.py", filename]
    subprocess.run(command)

if __name__ == "__main__":
    create_file()
    initBluetooth()
    while True:
        value   = read()
        if value != None:
            print(value)
            print(value["Load_cell"])
            current_time = datetime.datetime.now()
            update_file(value, current_time)