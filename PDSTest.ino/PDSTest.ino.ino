// this is the code for testing the parachute deployment system with the same avionics ebay for ion project v2

// the idea is simple: use the arm/disarm button to activate a regresive count and power the relay for deploying the parachute
#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP085.h>
#include <Adafruit_ADXL345_U.h>
#include <SD.h>


// conditional compilation: if serial defined all coms done through serial else through bluetooth
//#define ION_SERIAL // if set make sure bluetooth is not connected


// *** ******************************************* ***
// hardware needed defines and settings **************

// I2C communication: A4 ->SDA
// I2C communication: A5 ->SCL

Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345); /* Assign a unique ID to this sensor at the same time */
Adafruit_BMP085 bmp;                                              /* Barometer variable */

/*Bluetooth settings*/
#define bluetoothTx 5       // TX-O pin of bluetooth mate, Arduino D2
#define bluetoothRx 6       // RX-I pin of bluetooth mate, Arduino D3

SoftwareSerial bluetooth(bluetoothTx, bluetoothRx);

#define buzzer 10     // Buzzer pin
#define relay1 8      // Relay pin 8
#define relay2 9      // Relay pin 9

#define armedButton 2 // Arm / disarm button
#define armedLed 3    // armed led

/*
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4 (for MKRZero SD: SDCARD_SS_PIN)
 */
#define sdCard 4


// *** ******************************************* ***
// logical needed defines and settings **************

#define debounceTimeThreshold  250
#define LOGFILE       "PDSLog.txt"

// loging frequencies

#define LOGNOW  1
#define LOGFAST 5
#define LOGMED  20
#define LOGSLOW 60

// messages
#define DISARMED    0
#define ARMED       1
#define ARMED_READY 10
#define IN_FLIGHT   20
#define AT_APPOGE   30
#define DESCENT     35
#define DEPLOY_CHT  40
#define DEPLOY_ERR  45
#define GROUND_ZERO 50
#define NOT_READY   60
#define ACC_DETECT  70
#define ALT_DETECT  80
#define SD_DETECT   90

// changed the defines below to binary since the error varialble is unsigned short
#define NO_ERROR        0b000000      // errors should increment by mag orders of 10
#define ACCEL_ERROR     0b000001      // no ADXL345 detected or ADXL345 related error
#define ALTIM_ERROR     0b000010      // altimeter error
#define SERIALCOM_ERROR 0b000100      // Serial com Error
#define BLUETOOTH_ERROR 0b001000      // Bluetooth com Error
#define PARACHUTE_ERROR 0b010000      // Parachute deployment error
#define SD_ERROR        0b100000      // SD error

// function definitions
bool DeployPrimaryParachute (int altitude, short int InFlight, short int ReachedAppoge, short int ParachuteDeployed);
void arm(void);

// loging fucntions
void LogData(sensors_event_t acc, int altitude, bool InFlight, bool ParachuteDeployed,bool ReachedAppoge,short int msg, short int freq);
void serialLogData(short int msgs, short int freq, int data);

// config functions
void InitAccelerometer(void);
void InitComs(void);
void ConfigBlueTooth(void);
void InitSerial(void);
void ConfigInputOutput(void);
void InitSD(void);
void InitLogFile(void);

//Intterruption settings
volatile bool Armed = false;
//long debounceTimeCounter = 0;   moved to local variable 

// global variables
unsigned short Error = NO_ERROR;  // set if an error has been found.   init to 0 becasue all systems will be checked in setup

bool parachuteDeployed = false;
//long startTime = 0;         // time when armed
//long startMissionTime = 0;  // time when launch detected
//long appogeTime = 0;        // time when appoge was detected

void setup()
{
  ConfigInputOutput();  // set input output pins
  InitSD();     // init sd module so if everything fails we have error data stored in the sd card
  InitComs();   // first init comms if no comms nothing else works  
  InitAccelerometer();    
  
  // When the armed button is clicked, then the arm function is called to start a countdown
  attachInterrupt(digitalPinToInterrupt(armedButton), arm, FALLING);

  // done with setup.... proceed to main loop
  InitLogFile();
  serialLogData(ARMED_READY,LOGNOW,0);
}

void loop() { // While energized this loop will always run
  
  static bool ParachuteDeployed = false;  // true if parachute has been deployed
  unsigned int start;
  sensors_event_t event;                  // sensor acceleration variables
  accel.getEvent(&event);                 // get accelerometer event: 
  if (Armed) {        // start countdown
      LogData(event, 0,false, ParachuteDeployed,false,ARMED,LOGNOW);
      start=millis();
      while(millis()-start<10000){ // WAIT 10 seconds
        accel.getEvent(&event);                 // get accelerometer event: 
        LogData(event,0,false,ParachuteDeployed,false,ARMED,LOGNOW);
        delay(100);
      }
      ParachuteDeployed = DeployPrimaryParachute (0, false, false, ParachuteDeployed);
      accel.getEvent(&event);                 // get accelerometer event: 
      LogData(event,0,false,ParachuteDeployed,false,ARMED,LOGNOW);
      arm();
  } else { // while not armed just log slowly
     LogData(event, 0,false, ParachuteDeployed,false,DISARMED,LOGSLOW);
  } 
}

bool DeployPrimaryParachute (int altitude, short int InFlight, short int ReachedAppoge, short int ParachuteDeployed){
   // here is the code to set the parachute charge off
  sensors_event_t event; 
  accel.getEvent(&event);  
  LogData(event, altitude,InFlight, ParachuteDeployed,false,DEPLOY_CHT,LOGNOW);
  
  long start = millis();
  digitalWrite(relay1, LOW);
  while(millis()-start < 6000) {  // look at the next 6 secs while deploying
     accel.getEvent(&event);  
     LogData(event, altitude,InFlight, ParachuteDeployed,ReachedAppoge,DEPLOY_CHT,LOGNOW);
     delay(10);
  }
  digitalWrite(relay1, HIGH); // here we assume that the parachute has been deployed
  parachuteDeployed = true;
  accel.getEvent(&event);  
  if (true) {     // if parachute deployment has been detected
    LogData(event, altitude,InFlight, ParachuteDeployed,ReachedAppoge,DEPLOY_CHT,LOGNOW); 
    return true;
  }
  Error += PARACHUTE_ERROR;
  LogData(event, altitude,InFlight, ParachuteDeployed,ReachedAppoge,DEPLOY_ERR,LOGNOW); 
  return false;
}

void arm(void) {
  static long debounceTimeCounter = 0;  // moved to local instead of global
  if (millis() > debounceTimeCounter + debounceTimeThreshold) // check min elapsed time to toggle switch
  {
    Armed = !Armed;
    if (Armed) {
//      serialLogData(ARMED,LOGNOW);
    } else {
      serialLogData(DISARMED,LOGNOW,0);
    }    
    debounceTimeCounter = millis();
    digitalWrite(armedLed, Armed); // togle armed led on / off
  }
}



// loging fuctions
  
void LogData(sensors_event_t event, int altitude, bool InFlight, bool ParachuteDeployed,bool ReachedAppoge,short int msg,short int freq) {
  static short c = 0;
  File logFile;
  int acx = (int) (event.acceleration.x * 10.0); // first multiply by 10 then convert to int
  int acy = (int) (event.acceleration.y * 10.0);
  int acz = (int) (event.acceleration.z * 10.0);

  if (++c<freq) return; // dont do anything 4/5 times to be considered later
       #ifdef ION_SERIAL     // if serial coms defined then log to serial
           Serial.print(F(" alt: ")); Serial.print(altitude);
           Serial.print(F(" flt: ")); Serial.print(InFlight);
           Serial.print(F(" apg: ")); Serial.print(ReachedAppoge);
           Serial.print(F(" dpl: ")); Serial.print(ParachuteDeployed);
           Serial.print(F(" ERR: ")); Serial.print(Error,BIN);   
           switch(msg) {
             case DISARMED:     Serial.println(F("   Disarmed")); break;
             case ARMED_READY:  Serial.println(F("   Armed and Ready")); break;
             case IN_FLIGHT:    Serial.println(F("   In Flight"));       break;
             case AT_APPOGE:    Serial.println(F("   Reached appoge"));  break;
             case DESCENT:      Serial.println(F("   Descent stage"));   break;
             case DEPLOY_CHT:   Serial.println(F("   Deploying parachute")); break;
             case DEPLOY_ERR:   Serial.println(F("   ERROR deploying parachute")); break;
             default: Serial.println(F("   :::"));                       
           }       
        #else             // else log to bluetooth
           bluetooth.print(F(" alt: ")); bluetooth.print(altitude);
           bluetooth.print(F(" flt: ")); bluetooth.print(InFlight);
           bluetooth.print(F(" apg: ")); bluetooth.print(ReachedAppoge);
           bluetooth.print(F(" dpl: ")); bluetooth.print(ParachuteDeployed);
           bluetooth.print(F(" ERR: ")); bluetooth.print(Error,BIN);   
           switch(msg) {
             case DISARMED:     bluetooth.println(F("   Disarmed")); break;
             case ARMED_READY:  bluetooth.println(F("   Armed and Ready")); break;
             case IN_FLIGHT:    bluetooth.println(F("   In Flight"));       break;
             case AT_APPOGE:    bluetooth.println(F("   Reached appoge"));  break;
             case DESCENT:      bluetooth.println(F("   Descent stage"));   break;
             case DEPLOY_CHT:   bluetooth.println(F("   Deploying parachute")); break;
             case DEPLOY_ERR:   bluetooth.println(F("   ERROR deploying parachute")); break;
             default: bluetooth.println(F("   :::"));  
           }                     
        #endif    

  logFile = SD.open(LOGFILE, FILE_WRITE);    // Here log to file always
  logFile.print(millis()); 
  logFile.print(F("; "));  logFile.print(acx);
  logFile.print(F("; ")); logFile.print(acy);
  logFile.print(F("; ")); logFile.print(acz);
  logFile.print(F("; ")); logFile.print(altitude);
  logFile.print(F("; ")); logFile.print(InFlight);
  logFile.print(F("; ")); logFile.print(ReachedAppoge);
  logFile.print(F("; ")); logFile.print(ParachuteDeployed);  
  logFile.print(F("; ")); logFile.println(Error); 
  logFile.close();   // end log to file
          
  if(c>=LOGSLOW)
     c = 0;
}

void serialLogData(short int msg, short int freq,int data) {
  static short int c = 0;
  if (++c<freq) return; // dont do anything 4/5 times to be considered later
  #ifdef ION_SERIAL     // if serial coms defined then log to serial
  switch(msg){
    case DISARMED:    Serial.println(F("System Disarmed")); break;
    case ARMED:       Serial.println(F("System Armed")); break;
    case ARMED_READY: Serial.println(F("Setup Completed...  Systems GO")); break;
    case AT_APPOGE:   Serial.println(F("Reached appoge")); break;
    case ACC_DETECT:  Serial.println(F("ADXL 345 detected... values still incorrect")); break;
    case ALT_DETECT:  Serial.println(F("BMP 085 detected... values still incorrect")); break;
    case SD_DETECT:   Serial.println(F("SD module configured correctly")); break;
    case GROUND_ZERO: Serial.print(F("Average ground zero altitude: ")); Serial.println(data); break;
    case NOT_READY:   Serial.println(F("Not Armed, waiting")); break;
    default: break;   Serial.println(F("unknown call"));
  }
  #else             // else log to bluetooth
  switch(msg){
    case DISARMED:    bluetooth.println(F("System Disarmed")); break;
    case ARMED:       bluetooth.println(F("System Armed")); break;
    case ARMED_READY: bluetooth.println(F("Setup Completed...  Systems GO")); break;
    case AT_APPOGE:   bluetooth.println(F("Reached appoge")); break;
    case ACC_DETECT:  bluetooth.println(F("ADXL 345 detected... values still incorrect")); break;
    case ALT_DETECT:  bluetooth.println(F("BMP 085 detected... values still incorrect")); break;
    case SD_DETECT:   bluetooth.println(F("SD module configured correctly")); break;
    case GROUND_ZERO: bluetooth.print(F("Average ground zero altitude: ")); bluetooth.println(data); break;
    case NOT_READY:   bluetooth.println(F("Not Armed, waiting")); break;
    default: break;   bluetooth.println(F("unknown call"));
  }
  #endif   
  if(c>=LOGSLOW)
     c = 0;
}

// functions below are config functions... these only run if no com errors were found

void InitAccelerometer(void) { // Initialize ADXL 345 sensor....  this is done only once
  File logFile;
  if(!accel.begin()){ // There was a problem initializing the ADXL345 ... check your connections 
    Error += ACCEL_ERROR; // add accelerometer error  
  } else {
      serialLogData(ACC_DETECT,LOGNOW,0);
      logFile = SD.open(LOGFILE, FILE_WRITE); 
      logFile.println(F("Accelerometer initialized...."));
      logFile.close();
  }
}

// config coms... only configure Serial or bluetoot... if no serial is defined, bluetooth is configured

void InitLogFile(void){ // init the log file so it is ready for column loging... all data before was referential
  File logFile;
   
  logFile = SD.open(LOGFILE, FILE_WRITE); 
  logFile.println(F("time;   acx;   acy;   acz;   alt;   flt;   apg;   dpl;   ERR"));
  logFile.close(); 
}


void InitComs(void) {
  #if defined(ION_SERIAL) // if serial coms defined then init serial for debuging purposes
    InitSerial();
  #else
    ConfigBlueTooth();    // config bluetooth
  #endif
}

void InitSD(void) { // Initialie SD module... this is only done once.
  sensors_event_t aux;  
  File logFile;

  Serial.println(F("Entering SD init"));
  if (!SD.begin(sdCard)){ //There was a problem initializing the SD module.
    Error += SD_ERROR;
    //LogData(aux, 0, false, false,false,"error configuring SD module");          
  } else {
      // Now DELETE old file, create a new file and write the first line ever on the top of the file
       if (SD.exists(LOGFILE))
        SD.remove(LOGFILE);
      logFile = SD.open(LOGFILE, FILE_WRITE); 
      logFile.println(F("Initializing log file after powerup"));
      logFile.close();
      serialLogData(SD_DETECT,LOGNOW,0);     
  }
}

#if defined(ION_SERIAL) // if serial coms defined then this function is included
void InitSerial(void) { // Init serial com
  File logFile;
  #ifndef ESP8266
  while (!Serial); // wait for serial com initialization from for Leonardo/Micro/Zero
  #endif
  Serial.begin(9600);
  Serial.println(F("Innitializing serial coms"));
  while (Error) { // on error just stay here forever...   log, print, beep
    Serial.println(F("ERROR on setup.  Cannot continue: "));
    Serial.println(Error);
    delay(5);
  }
  logFile = SD.open(LOGFILE, FILE_WRITE); 
  logFile.println(F("Serial coms initialized..."));
  logFile.close();  
}
#else // if serial coms not defined then bluetooth is the default
void ConfigBlueTooth(void) { // config bluetooth coms
  File logFile;
  bluetooth.begin(115200);    // The Bluetooth Mate defaults to 115200bps
  bluetooth.print("$");       // Print three times individually
  bluetooth.print("$");
  bluetooth.print("$");       // Enter command mode
  delay(100);                 // Short delay, wait for the Mate to send back CMD
  bluetooth.println("U,9600,N");  // Temporarily Change the baudrate to 9600, no parity
  bluetooth.begin(9600);
  // how can we find an error when configuring bluetooth?
  
  logFile = SD.open(LOGFILE, FILE_WRITE); 
  logFile.println(F("Bluetooth initialized..."));
  logFile.close();
}
#endif

// aruduino hardware config

void   ConfigInputOutput(void) { // set input ouput arduino pins
  accel.setRange(ADXL345_RANGE_16_G);
  pinMode(buzzer, OUTPUT);
  pinMode(relay1, OUTPUT);
  pinMode(relay2, OUTPUT);
  digitalWrite(relay1, HIGH);
  digitalWrite(relay2, HIGH);
  pinMode(armedButton, INPUT_PULLUP);
  pinMode(armedLed, OUTPUT);
  digitalWrite(buzzer, HIGH);
}
